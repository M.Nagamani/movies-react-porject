import React, { Component } from "react";
import Adding from "./addDirector";
class Director extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      searchItem: [],
      isLoaded: false,
      directorMovies: [],
      showPopUp: false,
      target: "",
      updatedId: 0
    };
  }
  async componentDidMount() {
    console.log("working");
    await fetch("http://18.222.43.149:4000/api/directors/")
      .then(res => res.json())
      .then(data => {
        this.setState({
          items: data,
          searchItem: data,
          isLoaded: true
        });
      });
    await fetch(`http://18.222.43.149:4000/api/movies/`)
      .then(res => res.json())
      .then(data => {
        this.setState({
          directorMovies: data
        });
      });
  }
  handlePopUp = e => {
    this.setState({
      showPopUp: true,
      target: e
    });
  };
  handleAdding = e => {
    let data = {
      Director: e.target.parentNode.firstChild.value
    };
    console.log(data);
    if (this.state.updatedId == 0) {
      console.log(this.state.updatedId);
      fetch("http://18.222.43.149:4000/api/directors/", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
      }).then(res => {
        return this.componentDidMount();
      });
    } else {
      console.log(this.state.updatedId);
      fetch(`http://18.222.43.149:4000/api/directors/${this.state.updatedId}`, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data)
      }).then(res => {
        return this.componentDidMount();
      });
    }
    e.target.parentNode.firstChild.value = "";
  };
  handleDeletion = (e, id) => {
    fetch(`http://18.222.43.149:4000/api/directors/${id}`, {
      method: "DELETE"
    }).then(res => {
      return this.componentDidMount();
    });
  };
  handleEditing = (e, id) => {
    e.target.parentNode.parentNode.parentNode.querySelector(
      ".new-director input"
    ).value = e.target.parentNode.firstChild.innerHTML;
    this.setState({
      updatedId: id
    });
  };
  handleChange = e => {
    e.preventDefault();
    let searchItem = this.state.items;
    let searchElement = [...searchItem];
    if (e.target.value != "") {
      let elementSearched = searchElement.filter(element => {
        return (
          element.Director.toLowerCase().search(
            e.target.value.toLowerCase()
          ) !== -1
        );
      });
      this.setState({
        items: elementSearched
      });
    } else {
      let itemSearched = this.state.searchItem;
      this.setState({
        items: itemSearched
      });
    }
  };
  render() {
    return (
      <div>
        <div className="item">Directors</div>
        <div className="search-bar">
          <input
            type="text"
            placeholder="search here...."
            onChange={e => this.handleChange(e)}
          />
        </div>
        <div className="new-director">
          <input type="text" placeholder="enter director...." />
          <button onClick={e => this.handleAdding(e)}>Add Director</button>
        </div>
        <Adding
          myState={this.state}
          handleMovies={this.handlePopUp}
          editTheDirector={this.handleEditing}
          deletingDirector={this.handleDeletion}
        />
      </div>
    );
  }
}

export default Director;
