import React, { Component } from "react";
function DirectorWithMovies(props) {
  let director = props.value.map(element => {
    if (props.id == element.Director) {
      if (props.targeted == props.id) {
        if (props.pop == true) {
          return (
            <div key={element.Rank} className="director-movies">
              <div>{element.Title}</div>
            </div>
          );
        } else {
          return null;
        }
      }
    }
  });
  return director;
}
export default DirectorWithMovies;
