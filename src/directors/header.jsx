import React from "react";
import { Link } from "react-router-dom";
function Header() {
  return (
    <div>
      <h1>Movies And Directors</h1>
      <h3>
        <Link to="/movies">
          <span className="movies">Movies</span>
        </Link>
        <Link to="/director">
          <span className="director">Director</span>
        </Link>
      </h3>
    </div>
  );
}
export default Header;
