import React, { Component } from "react";
function Popup(props) {
  if (props.myValue.showPopUp == true) {
    return (
      <div className="pop-up">
        <div className="rank">
          <span>Rank :</span>
          <input type="text" placeholder="enter integer " />
        </div>
        <div className="title">
          <span>Title :</span>
          <input type="text" placeholder="enter string" />
        </div>
        <div className="description">
          <span>Description:</span>
          <input type="text" placeholder="enter string" />
        </div>
        <div className="runtime">
          <span>Runtime:</span>
          <input type="text" placeholder="enter integer " />
        </div>
        <div className="genre">
          <span>Genre:</span>
          <input type="text" placeholder="enter string " />
        </div>
        <div className="rating">
          <span>Rating:</span>
          <input type="text" placeholder="enter number" />
        </div>
        <div className="meta-score">
          <span>Metascore:</span>
          <input type="text" placeholder="enter number " />
        </div>
        <div className="votes">
          <span>Votes:</span>
          <input type="text" placeholder="enter number" />
        </div>
        <div className="gross">
          <span>Gross_Earning_in_Mil:</span>
          <input type="text" placeholder="enter number" />
        </div>
        <div className="pop-director">
          <span>Director:</span>
          <input type="text" placeholder="enter number" />
        </div>
        <div className="actor">
          <span>Actor:</span>
          <input type="text" placeholder="enter string" />
        </div>
        <div className="year">
          <span>Year:</span>
          <input type="text" placeholder="enter number" />
        </div>
        <div>
          <button onClick={e => props.addingNewMovie(e)}>Add NewMovie</button>
          <button onClick={e => props.closeThePopUp(e)}>Close</button>
        </div>
      </div>
    );
  } else return null;
}
export default Popup;
