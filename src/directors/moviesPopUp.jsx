import React, { Component } from "react";
function MoviesWithInfo(props) {
  let movies = props.value.map(element => {
    if (element.Rank == props.id) {
      if (props.targetId == element.Rank) {
        if (props.pop == true) {
          return (
            <div
              key={element.Rank}
              className="movies-detail"
              onClick={e => {
                props.handlePop(e);
              }}
            >
              <div>Rank:{element.Rank}</div>
              <div>Description:{element.Description}</div>
              <div>Runtime:{element.Runtime}</div>
              <div>Genre:{element.Genre}</div>
              <div>Rating:{element.Rating}</div>
              <div>Metascore:{element.Metascore}</div>
              <div>Votes:{element.Votes}</div>
              <div>Gross_Earning_in_Mil:{element.Gross_Earning_in_Mil}</div>
              <div>Director:{element.Director}</div>
              <div>Actor:{element.Actor}</div>
              <div>Year:{element.Year}</div>
            </div>
          );
        } else return null;
      }
    }
  });
  return movies;
}
export default MoviesWithInfo;
