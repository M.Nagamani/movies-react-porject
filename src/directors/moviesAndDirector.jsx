import React, { Component } from "react";
import Director from "./director";
import Movies from "./movies";
import Header from "./header";
import { BrowserRouter as Router, Route } from "react-router-dom";
class MoviesAndDirector extends Component {
  render() {
    return (
      <Router>
        <div>
          <Header />
          <Route path="/movies" component={Movies} />
          <Route path="/director" component={Director} />
        </div>
      </Router>
    );
  }
}
export default MoviesAndDirector;
