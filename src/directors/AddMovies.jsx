import React, { Component } from "react";
import MoviesWithInfo from "./moviesPopUp";
function AddingMovies(props) {
  let movies = props.myState.items.map(element => {
    return (
      <div key={element.Rank}>
        <div className="movies-names">
          <span onClick={e => props.handleMovies(element.Rank)}>
            {element.Title}
          </span>
          <button
            className="delete"
            onClick={e => props.deletingMovie(e, element.Rank)}
          >
            Delete
          </button>

          <button
            className="edit"
            onClick={e => props.editTheMovie(e, element.Rank)}
          >
            Edit
          </button>
        </div>
        <MoviesWithInfo
          pop={props.myState.MoviesPop}
          targetId={props.myState.targetRank}
          value={props.myState.items}
          id={element.Rank}
          handlePop={props.handlePop}
        />
      </div>
    );
  });
  return movies;
}
export default AddingMovies;
