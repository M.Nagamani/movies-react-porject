import React, { Component } from "react";
import AddingMovies from "./AddMovies";
import Popup from "./popUpForMovies";
class Movies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      searchItem: [],
      isLoaded: false,
      MoviesTitle: [],
      showPopUp: false,
      MoviesPop: false,
      targetRank: 0,
      updatedMovies: [],
      updatedId: 0
    };
  }
  componentDidMount() {
    console.log("working");
    fetch("http://18.222.43.149:4000/api/movies/")
      .then(res => res.json())
      .then(data => {
        this.setState({
          items: data,
          searchItem: data,
          isLoaded: true
        });
      });
  }
  fetchingMoviesDetails = () => {
    this.state.items.map(element => {
      fetch(`http://18.222.43.149:4000/api/movies/${element.Rank}`)
        .then(res => res.json())
        .then(data => {
          console.log(data);
          this.setState({
            MoviesTitle: data,
            isLoaded: false
          });
        });
    });
    console.log(this.state.MoviesTitle);
  };
  handlePopup = e => {
    this.setState({
      MoviesPop: true,
      targetRank: e
    });
  };
  handleAdding = e => {
    console.log("working");
    this.setState({
      showPopUp: true
    });
  };
  handleClosing = e => {
    this.setState({
      showPopUp: false
    });
  };
  handleDeleting = (e, id) => {
    fetch(`http://18.222.43.149:4000/api/movies/${id}`, {
      method: "DELETE"
    }).then(res => {
      return this.componentDidMount();
    });
  };
  addingMovie = e => {
    let targeted = e.target.parentNode.parentNode;
    let data = {
      Rank: targeted.querySelector(".rank input").value,
      Title: targeted.querySelector(".title input").value,
      Description: targeted.querySelector(".description input").value,
      Runtime: targeted.querySelector(".runtime input").value,
      Genre: targeted.querySelector(".genre input").value,
      Rating: targeted.querySelector(".rating input").value,
      Metascore: targeted.querySelector(".meta-score input").value,
      Votes: targeted.querySelector(".votes input").value,
      Gross_Earning_in_Mil: targeted.querySelector(".gross input").value,
      Director: targeted.querySelector(".pop-director input").value,
      Actor: targeted.querySelector(".actor input").value,
      Year: targeted.querySelector(".year input").value
    };
    if (this.state.updatedId == 0) {
      fetch("http://18.222.43.149:4000/api/movies", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
      }).then(res => {
        return this.componentDidMount();
      });
    } else {
      fetch(`http://18.222.43.149:4000/api/movies/${this.state.updatedId}`, {
        method: "PUT",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json; charset=UTF-8"
          },
          body: JSON.stringify(data)
        }).then(res => {
          return this.componentDidMount();
        });
    }
    targeted.querySelector(".rank input").value = "";
    targeted.querySelector(".title input").value = "";
    targeted.querySelector(".description input").value = "";
    targeted.querySelector(".runtime input").value = "";
    targeted.querySelector(".genre input").value = "";
    targeted.querySelector(".rating input").value = "";
    targeted.querySelector(".meta-score input").value = "";
    targeted.querySelector(".votes input").value = "";
    targeted.querySelector(".gross input").value = "";
    targeted.querySelector(".pop-director input").value = "";
    targeted.querySelector(".actor input").value = "";
    this.setState({
      showPopUp: false
    });
  };
  handleEditing = (e, id) => {
    let updateMovies = [];
    this.state.items.map(element => {
      if (element.Rank == id) {
        updateMovies.push(element);
      }
    });
    let popTarget = e.target;
    this.setState(
      {
        updatedId: id,
        showPopUp: true
      },
      () => {
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".rank input"
        ).value = updateMovies[0].Rank;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".title input"
        ).value = updateMovies[0].Title;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".description input"
        ).value = updateMovies[0].Description;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".runtime input"
        ).value = updateMovies[0].Runtime;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".genre input"
        ).value = updateMovies[0].Genre;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".rating input"
        ).value = updateMovies[0].Rating;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".meta-score input"
        ).value = updateMovies[0].Metascore;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".votes input"
        ).value = updateMovies[0].Votes;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".gross input"
        ).value = updateMovies[0].Gross_Earning_in_Mil;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".pop-director input"
        ).value = updateMovies[0].Director;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".actor input"
        ).value = updateMovies[0].Actor;
        popTarget.parentNode.parentNode.parentNode.querySelector(
          ".year input"
        ).value = updateMovies[0].Year;
      }
    );
  };
  handleMoviesDetails = e => {
    this.setState({
      MoviesPop: false
    });
  };
  handleChange = e => {
    e.preventDefault();
    let searchItem = this.state.items;
    let searchElement = [...searchItem];
    if (e.target.value != "") {
      let elementSearched = searchElement.filter(element => {
        return (
          element.Title.toLowerCase().search(e.target.value.toLowerCase()) !==
            -1 ||
          element.Actor.toLowerCase().search(e.target.value.toLowerCase()) !==
            -1 ||
          element.Description.toLowerCase().search(
            e.target.value.toLowerCase()
          ) !== -1 ||
          element.Rating.toLowerCase().search(e.target.value.toLowerCase()) !==
            -1
        );
      });
      this.setState({
        items: elementSearched
      });
    } else {
      let itemSearched = this.state.searchItem;
      this.setState({
        items: itemSearched
      });
    }
  };
  render() {
    // this.fetchingMoviesDetails();
    return (
      <div>
        <div className="movies-items">Movies</div>
        <div className="new-movies">
          <input
            type="text"
            placeholder="search here...."
            onChange={e => {
              this.handleChange(e);
            }}
          />
          <button onClick={e => this.handleAdding(e)}>Add Movies</button>
        </div>
        <Popup
          myValue={this.state}
          addingNewMovie={this.addingMovie}
          closeThePopUp={this.handleClosing}
        />
        <AddingMovies
          myState={this.state}
          handleMovies={this.handlePopup}
          deletingMovie={this.handleDeleting}
          editTheMovie={this.handleEditing}
          handlePop={this.handleMoviesDetails}
        />
      </div>
    );
  }
}
export default Movies;
