import React, { Component } from "react";
import DirectorWithMovies from "./directorInfo";
function Adding(props) {
  // console.log(props.myState.target);
  let addingDirector = props.myState.items.map(element => {
    return (
      <div key={element.Director_id} className={element.Director_id}>
        <div className="director-name">
          <span onClick={e => props.handleMovies(element.Director_id)}>
            {element.Director}
          </span>
          <button
            className="delete"
            onClick={e => props.deletingDirector(e, element.Director_id)}
          >
            Delete
          </button>

          <button
            className="edit"
            onClick={e => props.editTheDirector(e, element.Director_id)}
          >
            Edit
          </button>
        </div>
        <DirectorWithMovies
          targeted={props.myState.target}
          pop={props.myState.showPopUp}
          value={props.myState.directorMovies}
          id={element.Director_id}
        />
      </div>
    );
  });
  return addingDirector;
}
export default Adding;
